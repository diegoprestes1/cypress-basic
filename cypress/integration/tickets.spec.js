describe('Tickets', () => {
  beforeEach(() => {
    cy.visit('https://bit.ly/2XSuwCW')
  });

  it('fills all the text input fields', () => {
    const firstName = 'Diego';
    const lastName = 'Prestes';
    cy.get('#first-name').type(firstName);
    cy.get('#last-name').type(lastName);
    cy.get('#email').type('diego@email.com');
    cy.get('#requests').type('Water');
    cy.get('#signature').type(`${firstName} ${lastName}`);
  });

  it('select two tickets', () => {
    cy.get('#ticket-quantity').select('2');
  });

  it('select vip ticket type', () => {
    cy.get('#vip').check('vip');
  });

  it('selects friend and social media checkbox', () => {
    cy.get('#friend').check();
    cy.get('#social-media').check();
  });

  it('selects friend and publication, then uncheck friend', () => {
    cy.get('#friend').check();
    cy.get('#publication').check();
    cy.get('#friend').uncheck();
  });

  it('has TICKETBOX headers heading', () => {
    cy.get('header h1').should('contain', 'TICKETBOX');
  });

  it('alerts on invalid email', () => {
    cy.get('#email')
      .as('email')
      .type('diego-email.com');

    cy.get('#email.invalid').should('exist');

    cy.get('@email')
      .clear()
      .type('diego@email.com');
    
    cy.get('#email.invalid').should('not.exist');
  });

  it('fills and reset the form', () => {
    const firstName = 'Diego';
    const lastName = 'Prestes';
    const fullName = `${firstName} ${lastName}`;

    cy.get('#first-name').type(firstName);
    cy.get('#last-name').type(lastName);
    cy.get('#email').type('diego@email.com');
    cy.get('#ticket-quantity').select('2');
    cy.get('#vip').check('vip');
    cy.get('#friend').check();
    cy.get('#requests').type('Water');

    cy.get('.agreement p').should(
      'contain',
      `I, ${fullName}, wish to buy 2 VIP tickets.`
    );

    cy.get('#agree').click();
    cy.get('#signature').type(fullName);

    cy.get('button[type="submit"]')
      .as('submitButton')
      .should('not.be.disabled');
    
    cy.get('button[type="reset"]').click();
      
    cy.get('@submitButton').should('be.disabled');
  });

  it('fills mandatory fields using support command', () => {
    const customer = {
      firstName: 'Diego',
      lastName: 'Prestes',
      email: 'diego@email.com'
    };

    cy.fillMandatoryFields(customer);

    cy.get('button[type="submit"]')
      .as('submitButton')
      .should('not.be.disabled');
    
    cy.get('#agree').uncheck();
      
    cy.get('@submitButton').should('be.disabled');
  });
});